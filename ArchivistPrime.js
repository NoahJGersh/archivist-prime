const {Client, Intents} = require('discord.js');
const config = require('./config.json');
const client = new Client({intents: [Intents.FLAGS.GUILDS]});

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('interactionCreate', async (interaction) => {
  if (!interaction.isCommand()) return;

  if (interaction.commandName === 'ping') {
    await interaction.reply('Pong!');
  } else if (interaction.commandName === 'define') {
    await interaction.reply('Got command with options ' + interaction.options);
  }
});

client.login(config.token);