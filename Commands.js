const {REST} = require('@discordjs/rest');
const {Routes} = require('discord-api-types/v9');

const commands = [
  {
    name: 'ping',
    description: 'Replies with Pong!',
  },
  {
    name: 'define',
    description: 'Get the definition of a word in a Vastest Sea language',
    options: [
      {
        name: 'language',
        description: 'The language of origin',
        required: true,
        choices: [
          {
            'name': 'Fjorunskara',
            'value': 'fjorunskara',
          },
        ],
        type: 'STRING',
      },
      {
        name: 'word',
        description: 'The word to translate into English',
        required: true,
        type: 'STRING',
      },
    ],
  },
];

const rest = new REST({version: '9'}).setToken('token');

(async () => {
  try {
    console.log('Started refreshing application (/) commands.');

    await rest.put(
      Routes.applicationGuildCommands(CLIENT_ID, GUILD_ID),
      {body: commands},
    );

    console.log('Successfully reloaded application (/) commands.');
  } catch (error) {
    console.error(error);
  }
})();