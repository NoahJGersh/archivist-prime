const {SlashCommandBuilder} = require('@discordjs/builders');
const {MessageEmbed} = require('discord.js');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('define')
    .setDescription('Define a word from a given language.')
    .addStringOption(
      (option) => {
        return option.setName('language')
          .setDescription('The language of origin')
          .setRequired(true)
          .addChoice('Fjorunskara', 'Fjorunskara');
      }
    )
    .addStringOption(
      (option) => {
        return option.setName('word')
          .setDescription('The word to define')
          .setRequired(true);
      }
    ),
  async execute(interaction) {
    const lang = interaction.options.getString('language');
    const word = interaction.options.getString('word');
    fetch('https://noahger.sh/v0/languages/' + lang.toLowerCase() + '/lexicon/' + word, {
      method: 'GET',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
    })
      .then((res) => {
        if (!res.ok) throw res;
        return res.json();
      })
      .then((json) => {
        const embed = new MessageEmbed()
          .setColor('#00cccc')
          .setTitle(`Definition - ${json.name_roman}`)
          .setURL(`https://noahger.sh/vastestsea/languages/${lang.toLowerCase()}/lexicon#${json.name_roman}`)
          .addFields(
            {name: 'Romanized', value: json.name_roman, inline: true},
            {name: 'Glyph', value: json.name_glyph, inline: true},
            {name: 'Language of Origin', value: lang},
            {name: 'Part of Speech', value: json.part},
            {name: 'Definition', value: json.definition},
          )
          .setTimestamp();
        interaction.reply({embeds: [embed]});
      })
      .catch((error) => {
        console.error(error);
        interaction.reply(`Could not find a definition for ${word.toLowerCase()} in ${lang}.`);
      });
  },
};